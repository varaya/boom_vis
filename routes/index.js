// Require
const express = require('express');

// Router
const router = express.Router();

const {
    init,
    getVolcanoes,
    getEvents,
    getSamples,
    getExtent,
    getAnalysisByAuthors,
} = require('../controllers/index');

router
    .get('/', init)
    .get('/api/volcanoes', getVolcanoes)
    .get('/api/events', getEvents)
    .get('/api/samples', getSamples)
    .get('/api/extent', getExtent)
    .get('/api/analysis-by-authors', getAnalysisByAuthors)

module.exports = router;