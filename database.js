// This is a bit horrible, sorry future self

var sqlite3 = require('sqlite3').verbose()
const csv = require('csv-parser');
const fs = require('fs');


const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
  if (err) {
    // Cannot open database
    console.error(err.message)
    throw err
  } else {
    console.log('Connected to the SQLite database.')
    createVolcanoes(db)
  }
});

let createVolcanoes = function (db) {
  const volcanoes = {}
  fs.createReadStream('./data/Volcanes.csv')
    .pipe(csv())
    .on('data', (row) => {
      volcanoes[row['Volcano']] = row
    })
    .on('end', () => {
      db.run(`CREATE TABLE volcano (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            Name text, 
            Latitude real, 
            Longitude real, 
            Color text
            )`,
        (err) => {
          if (err) {
            // Table already created
            //console.log(err)
          } else {
            console.log('Creating Volcanoes Table')
            // Table just created, creating some rows
            var insert = 'INSERT INTO volcano (Name, Latitude, Longitude, Color) VALUES (?,?,?,?)'
            var id = 1;
            for (let v in volcanoes) {
              volcan = volcanoes[v];
              db.run(insert, [volcan.Volcano, volcan.Latitude, volcan.Longitude, volcan.Color])
              volcan.id = id
              id += 1
            }
            console.log('Volcanoes table ready');
            createEvents(db, volcanoes)
          }
        });
    });
}


let createEvents = function (db, volcanoes) {
  const events = {}
  const extents = {}
  const samples = []
  fs.createReadStream('./data/TephraDataBase.csv')
    .pipe(csv())
    .on('data', (row) => {
      // COMPUTE EVENTS
      let v = row.Volcano
      let e = row.Event
      if (!(v in events)) {
        events[v] = {}
        extents[v] = {}
        //measuredMaterial[v] = {}
      }
      if (!(e in events[v])) {
        events[v][e] = {}
        events[v][e].Event = e
        events[v][e].Volcano = v
        events[v][e].HistoricalAge = []
        events[v][e].Ages14C = []
        events[v][e].Magnitudes = {}
        events[v][e].Veis = {}
        //measuredMaterial[v][e] = {}
      }

      // COMPUTE EVENTS TIMELINES
      if (row['14C_Age'] !== '' && !isNaN(row['14C_Age'])) {
        const age14C = -(Number(row['14C_Age']))
        const ageError14C = Number(row['14C_Age_Error'])
        row['14C_Age_Num'] = age14C
        row['14C_Age_Error_Num'] = ageError14C
        events[v][e].Ages14C.push(age14C - ageError14C)
        events[v][e].Ages14C.push(age14C + ageError14C)
      }

      // COMPUTE EVENTS TIMELINES
      if (row['HistoricalAge'] !== '' && !isNaN(row['HistoricalAge'])) {
        const HistoricalAge = (Number(row['HistoricalAge']))
        events[v][e].HistoricalAge.push(HistoricalAge)
      }

      // COMPUTE EVENTS MAGNITUDE
      let m = row.Magnitude
      if (m !== '' && !isNaN(m)) {
        // I assume there is only one per author
        events[v][e].Magnitudes[row.Authors] = Number(m)
      }

      // COMPUTE EVENTS VEI
      let vei = row.Vei
      if (vei !== '' && !isNaN(vei)) {
        // I'm doing this by sampleID instead of authors
        events[v][e].Veis[row.SampleID] = Number(vei)
      }

      // COMPUTE DOI YEAR, JUST FOR CONVENIENCE
      row.DOIYear = getDOIYear(row.Authors)
      samples.push(row)
    })
    .on('end', () => {
      db.run(`CREATE TABLE event (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            Name text, 
            Volcano text,
            VolcanoID integer,
            HistoricalAge integer,
            MinAge14C real, 
            MaxAge14C real, 
            Magnitude real,
            Vei integer,
            FOREIGN KEY(VolcanoID) REFERENCES volcano(id)
            )`,
        (err) => {
          if (err) {
            // Table already created
            // console.log(err)
          } else {
            console.log('Creating events table')
            var id = 1
            for (let v in events) {
              for (let e in events[v]) {
                const event = events[v][e];
                if (!(v in volcanoes))
                  continue
                events[v][e].MaxAge14C = Math.max(...event.Ages14C);
                events[v][e].MinAge14C = Math.min(...event.Ages14C);
                events[v][e].HistoricalAge = Math.min(...event.HistoricalAge);

                // Calculate the mode (consider only one per publication) for the magnitude
                // There must be a better way to do this, but I'm too lazy to think 
                // It's almost holidays!
                const mags = {}
                for (let a in events[v][e].Magnitudes) {
                  const aMag = events[v][e].Magnitudes[a]
                  if (!(aMag in mags)) {
                    mags[aMag] = 0
                  }
                  mags[aMag] += 1
                }
                var maximaMag = ''
                var maximaModeMag = 0
                for (let aMag in mags) {
                  const rep = mags[aMag]
                  if (rep > maximaModeMag) {
                    maximaModeMag = rep
                    maximaMag = aMag
                  }
                }
                if (maximaMag != '') {
                  events[v][e].Magnitude = maximaMag

                }

                // Calculate the mode (consider only one per publication) for the VEI too
                // Same lazy approach
                const veis = {}
                for (let a in events[v][e].Veis) {
                  const aVei = events[v][e].Veis[a]
                  if (!(aVei in veis)) {
                    veis[aVei] = 0
                  }
                  veis[aVei] += 1
                }
                var maximaVei = ''
                var maximaModeVei = 0
                for (let aVei in veis) {
                  const rep = veis[aVei]
                  if (rep > maximaModeVei) {
                    maximaModeVei = rep
                    maximaVei = aVei
                  }
                }
                if (maximaVei != '') {
                  events[v][e].Vei = maximaVei
                }

                var insert = 'INSERT INTO event (Name, Volcano, VolcanoID, HistoricalAge, MinAge14C, MaxAge14C, Magnitude, Vei) VALUES (?,?,?,?,?,?,?,?)'
                var id_volcan = volcanoes[v].id
                db.run(insert, e, v, id_volcan, Number(events[v][e].HistoricalAge), events[v][e].MinAge14C, events[v][e].MaxAge14C, events[v][e].Magnitude, events[v][e].Vei)
                events[v][e].id = id
                id += 1
              }
            }
            console.log('Events table ready');
            createSamples(volcanoes, events, samples)
          }
        });
    });
}

let createSamples = function (volcanes, eventos, muestras) {
  var columns = `Volcano text, Event text, Magnitude real, Vei text, ISGN text, Location text,
Latitude real, Longitude real, Authors text, DOI text, DOIYear integer, TypeOfRegister text, TypeOfAnalysis text,
AnalyzedMaterial text, TypeOfSection text, SectionID text, SubSectionID text, SubSection_DistanceFromTop text,
HistoricalAge text, "14C_Age" text, "14C_Age_Error" text, "14C_Age_Num" real,  "14C_Age_Error_Num" real,
"14CLabCode" text, StratigraphicPosition text, "40Ar39Ar_Age" text, "40Ar39Ar_Age_Error" text,
DepositColor text, DepositThickness_cm text, GrainSize_min_mm text, GrainSize_max_mm text, 
SampleID text, SampleObservationID text, AnalyticalTechnique text, MeasurementRun text, Comments text,
Flag text, FlagDescription text, Map text, 
SiO2 real, TiO2 real, Al2O3 real, FeO real, Fe2O3 real, Fe2O3T real, FeOT real, MnO real, MgO real, CaO real,
Na2O real, K2O real, P2O5 real, Cl real, H2O real, LOI real, Total real, Rb real, Sr real, Y real, Zr real, 
Nb real, Cs real, Ba real, La real, Ce real, Pr real, Nd real, Sm real, Eu real, Gd real, Tb real, Dy real,
Ho real, Er real, Tm real, Yb real, Lu real, Hf real, Ta real, Pb real, Th real, U real, "La_Yb" real,
"Zr_Nb" real, "87Sr_86Sr" real, "2SE_87Sr_86Sr" real, "143Nd_144Nd" real, "2SE_143Nd_144Nd" real,
SiO2_normalized real, TiO2_normalized real, Al2O3_normalized real, FeO_normalized real, Fe2O3_normalized real,
Fe2O3T_normalized real, FeOT_normalized real, MnO_normalized real, MgO_normalized real, CaO_normalized real,
Na2O_normalized real, K2O_normalized real, P2O5_normalized real, Cl_normalized real, Total_normalization real,
VolcanoID integer, EventID integer, `
  // I remove new lines 
  columns = columns.replace(/[\n\r\t]/g, '');

  var columns_clear = `Volcano, Event, Magnitude, Vei, ISGN, Location,
Latitude, Longitude, Authors, DOI, DOIYear, TypeOfRegister, TypeOfAnalysis,
AnalyzedMaterial, TypeOfSection, SectionID, SubSectionID, SubSection_DistanceFromTop,
HistoricalAge, "14C_Age", "14C_Age_Error", "14C_Age_Num",  "14C_Age_Error_Num",
"14CLabCode", StratigraphicPosition, "40Ar39Ar_Age", "40Ar39Ar_Age_Error",
DepositColor, DepositThickness_cm, GrainSize_min_mm, GrainSize_max_mm, 
SampleID, SampleObservationID, AnalyticalTechnique, MeasurementRun, Comments,
Flag, FlagDescription, Map, 
SiO2, TiO2, Al2O3, FeO, Fe2O3, Fe2O3T, FeOT, MnO, MgO, CaO,
Na2O, K2O, P2O5, Cl, H2O, LOI, Total, Rb, Sr, Y, Zr, 
Nb, Cs, Ba, La, Ce, Pr, Nd, Sm, Eu, Gd, Tb, Dy,
Ho, Er, Tm, Yb, Lu, Hf, Ta, Pb, Th, U, "La_Yb",
"Zr_Nb", "87Sr_86Sr", "2SE_87Sr_86Sr", "143Nd_144Nd", "2SE_143Nd_144Nd",
SiO2_normalized, TiO2_normalized, Al2O3_normalized, FeO_normalized, Fe2O3_normalized,
Fe2O3T_normalized, FeOT_normalized, MnO_normalized, MgO_normalized, CaO_normalized,
Na2O_normalized, K2O_normalized, P2O5_normalized, Cl_normalized, Total_normalization,
VolcanoID, EventID `
  columns_clear = columns_clear.replace(/[\n\r\t]/g, '');

  var sql_create = "CREATE TABLE sample (id INTEGER PRIMARY KEY AUTOINCREMENT,"
  sql_create += columns
  sql_create += 'FOREIGN KEY(VolcanoID) REFERENCES volcano(id), FOREIGN KEY(EventID) REFERENCES event(id))'

  db.run(sql_create,
    (err) => {
      if (err) {
        // Table already created
        console.log(err)
      } else {
        console.log('Creating samples table')
        muestras.forEach(m => {
          if (!(m.Volcano in volcanes) || !(m.Event in eventos[m.Volcano]))
            return
          var insert = 'INSERT INTO sample (' + columns_clear + ') VALUES ('
          for (let i = 0; i < 104; i++) {
            insert += '?,'
          }
          insert += '?)'
          var edadNum = -1
          var errorEdadNum = -1
          if (m['14C_Age'] !== '' && !isNaN(m['14C_Age'])) {
            edadNum = -(Number(m['14C_Age']))
            errorEdadNum = Number(m['14C_Age_Error'])
          }
          db.run(insert,
            m.Volcano,
            m.Event,
            m.Magnitude,
            m.Vei,
            m.ISGN,
            m.Location,
            m.Latitude,
            m.Longitude,
            m.Authors,
            m.DOI,
            m.DOIYear,
            m.TypeOfRegister,
            m.TypeOfAnalysis,
            m.AnalyzedMaterial,
            m.TypeOfSection,
            m.SectionID,
            m.SubSectionID,
            m.SubSection_DistanceFromTop,
            m.HistoricalAge,
            m["14C_Age"],
            m["14C_Age_Error"],
            m["14C_Age_Num"],
            m["14C_Age_Error_Num"],
            m["14CLabCode"],
            m.StratigraphicPosition,
            m["40Ar39Ar_Age"],
            m["40Ar39Ar_Age_Error"],
            m.DepositColor,
            m.DepositThickness_cm,
            m.GrainSize_min_mm,
            m.GrainSize_max_mm,
            m.SampleID,
            m.SampleObservationID,
            m.AnalyticalTechnique,
            m.MeasurementRun,
            m.Comments,
            m.Flag,
            m.FlagDescription,
            m.Map,
            m.SiO2,
            m.TiO2,
            m.Al2O3,
            m.FeO,
            m.Fe2O3,
            m.Fe2O3T,
            m.FeOT,
            m.MnO,
            m.MgO,
            m.CaO,
            m.Na2O,
            m.K2O,
            m.P2O5,
            m.Cl,
            m.H2O,
            m.LOI,
            m.Total,
            m.Rb,
            m.Sr,
            m.Y,
            m.Zr,
            m.Nb,
            m.Cs,
            m.Ba,
            m.La,
            m.Ce,
            m.Pr,
            m.Nd,
            m.Sm,
            m.Eu,
            m.Gd,
            m.Tb,
            m.Dy,
            m.Ho,
            m.Er,
            m.Tm,
            m.Yb,
            m.Lu,
            m.Hf,
            m.Ta,
            m.Pb,
            m.Th,
            m.U,
            m["La_Yb"],
            m["Zr_Nb"],
            m["87Sr_86Sr"],
            m["2SE_87Sr_86Sr"],
            m["143Nd_144Nd"],
            m["2SE_143Nd_144Nd"],
            m.SiO2_normalized,
            m.TiO2_normalized,
            m.Al2O3_normalized,
            m.FeO_normalized,
            m.Fe2O3_normalized,
            m.Fe2O3T_normalized,
            m.FeOT_normalized,
            m.MnO_normalized,
            m.MgO_normalized,
            m.CaO_normalized,
            m.Na2O_normalized,
            m.K2O_normalized,
            m.P2O5_normalized,
            m.Cl_normalized,
            m.Total_normalization,
            volcanes[m.Volcano].id,
            eventos[m.Volcano][m.Event].id,
            (err) => {
              if (err) {
                console.log(err)
              } else {

              }
            })
        })

        console.log('Samples table ready');
      }
    })
}

var getDOIYear = function (ref) {
  var year = ''
  const lastChar = ref.slice(-1)
  if (lastChar == 'a' || lastChar == 'b') {
    ref = ref.slice(0, -1)
  }
  year = ref.slice(-4) // I'm assuming is always 4 year digits
  return Number(year)
}

module.exports = db