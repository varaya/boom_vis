# TephraDatabase

DOI: https://doi.org/10.14768/47b4525f-ff39-4940-a963-4d2673f2362e
Datasets: https://data.ipsl.fr/repository/TephraDatabase/

# Installation
To install node modules use this command:
```
npm i
```
# Start
To start the application use this command:

```
npm start
```

Now, to see the app, open a browser and go to this link

> localhost:3001

# License
[MIT](https://choosealicense.com/licenses/mit/)


