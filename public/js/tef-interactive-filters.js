import { loading, ready } from './tef-utils.js'
export default function interactiveFilters() {
    var interactiveFilters = {}
    var _ndx
    var _tef

    interactiveFilters.init = function (tef, ndx) {
        _ndx = ndx
        _tef = tef
        createTypeRegisterBarChart()
        createTypeOfAnalysisBarChart()
        createTypeOfSectionBarChart()
        createFlagBarChart()

    }

    var createTypeRegisterBarChart = function () {
        const typeRegisterBarchart = new dc.RowChart('#type-register-chart');
        const registerDimension = _ndx.dimension(d => d.TypeOfRegister);
        const registerGroup = registerDimension.group()
        typeRegisterBarchart.width(350)
            .height(250)
            .margins({ top: 0, right: 50, bottom: 20, left: 40 })
            .group(registerGroup)
            .dimension(registerDimension)
            .on('filtered.monitor', function (chart, filter) {
                _tef.filter('TypeOfRegister', chart.filters())
            })
            .elasticX(true)
            .title(d => d.value)
            .xAxis()
            .ticks(4)
        typeRegisterBarchart.onClick = function (datum) {
            loading()
            setTimeout(() => {
                const filter = typeRegisterBarchart.keyAccessor()(datum);
                typeRegisterBarchart.filter(filter);
            }, 500)
        }
        typeRegisterBarchart.render()
    }

    var createTypeOfAnalysisBarChart = function () {
        const typeAnalysisBarchart = new dc.RowChart('#type-analysis-chart');
        const analysisDimension = _ndx.dimension(d => d.TypeOfAnalysis);
        const analysisGroup = analysisDimension.group()
        typeAnalysisBarchart.width(350)
            .height(250)
            .margins({ top: 0, right: 50, bottom: 20, left: 40 })
            .group(analysisGroup)
            .dimension(analysisDimension)
            .on('filtered.monitor', function (chart, filter) {
                _tef.filter('TypeOfAnalysis', chart.filters())
            })
            .elasticX(true)
            .title(d => d.value)
            .xAxis()
            .ticks(4)
        typeAnalysisBarchart.onClick = function (datum) {
            loading()
            setTimeout(() => {
                const filter = typeAnalysisBarchart.keyAccessor()(datum);
                typeAnalysisBarchart.filter(filter);
            }, 500)
        }
        typeAnalysisBarchart.render()
    }

    var createTypeOfSectionBarChart = function () {
        var typeSectionBarchart = new dc.RowChart('#type-section-chart');
        var sectionDimension = _ndx.dimension(d => d.TypeOfSection);
        var sectionGroup = sectionDimension.group()
        typeSectionBarchart.width(350)
            .height(250)
            .margins({ top: 0, right: 50, bottom: 20, left: 40 })
            .group(sectionGroup)
            .dimension(sectionDimension)
            .on('filtered.monitor', function (chart, filter) {
                _tef.filter('TypeOfSection', chart.filters())
            })
            .elasticX(true)
            .title(d => d.value)
            .xAxis()
            .ticks(4)
        typeSectionBarchart.onClick = function (datum) {
            loading()
            setTimeout(() => {
                const filter = typeSectionBarchart.keyAccessor()(datum);
                typeSectionBarchart.filter(filter);
            }, 500)
        }
        typeSectionBarchart.render()
    }

    var createFlagBarChart = function () {
        function reduceAdd(p, v, nf) {
            v.Flag.forEach(f => {
                if (!(f in p))
                    p[f] = 0
                ++p[f]
            })
            return p
        }

        function reduceRemove(p, v, nf) {
            v.Flag.forEach(f => {
                if (!(f in p))
                    p[f] = 0
                --p[f]
            })
            return p;
        }

        function reduceInitial() {
            var r = {}
            return r;
        }

        var flagsBarchart = new dc.RowChart('#flags-chart');
        var flagDimension = _ndx.dimension(d => d.Flag, true);
        var flagGroup = flagDimension.group().reduce(reduceAdd, reduceRemove, reduceInitial)
        flagsBarchart.width(350)
            .height(250)
            .margins({ top: 0, right: 50, bottom: 20, left: 40 })
            .group(flagGroup)
            .dimension(flagDimension)
            .elasticX(true)
            .valueAccessor(d => { return d.value[d.key] })
            .on('filtered.monitor', function (chart, filter) {
                _tef.filter('Flag', chart.filters())
            })
            .title(d => d.value[d.key])
            .xAxis()
            .ticks(4)
        flagsBarchart.onClick = function (datum) {
            loading()
            setTimeout(() => {
                const filter = flagsBarchart.keyAccessor()(datum);
                flagsBarchart.filter(filter);
            }, 500)
        }
        flagsBarchart.render()
    }

    return interactiveFilters
}