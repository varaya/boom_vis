import { sampleObservationInfo, eventInfo } from './tef-info-content.js'


///////////////////////////////////
// TOOLTIPS FOR MOUSE OVER AND OUT

function labelTooltipText(d) {
  var tipText = '<text class="titulo-tooltip">'
  tipText += d
  tipText += '</text>'
  return tipText
}

export function tooltipMouseOver(dataType, d, event) {
  d3.select(this).style("cursor", "pointer"); 
  d3
    .select('.tef-tooltip')
    .transition()
    .duration(200)
    .style('opacity', 1)

  var tipText
  if (dataType == 'sampleObservation') {
    tipText = sampleObservationInfo(d)
  } else if (dataType == 'event') {
    tipText = eventInfo(d)
  } else {
    tipText = labelTooltipText(d)
  }

  d3
    .select('.leaflet-popup-content')
    .html(tipText)
  d3
    .select('.leaflet-popup')
    .style('left', (event.pageX + 25) + 'px')
    .style('top', event.pageY - 28 + 'px')
}

export function tooltipMouseOut() {
  d3
    .select('.leaflet-popup')
    .style('left', -1000 + 'px')
    .style('top', -1000 + 'px')
  d3
    .select('.tef-tooltip')
    .transition()
    .duration(100)
    .style('opacity', 0)
}

export function tooltipClose() {
  d3
    .select('.tef-tooltip')
    .transition()
    .duration(500)
    .style('opacity', 0)
}

///////////////////////////////
// CLICK ON ELEMENTS

export function sampleClick(text) {
    d3
        .select('#clickedSamplesInfo')
        .html(text)
  }