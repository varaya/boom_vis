import map from './tef-map.js'
import timeline from './tef-timeline.js'
import table from './tef-table.js'
import scatterplot from './tef-scatterplot.js'
import interactiveFilters from './tef-interactive-filters.js'
import dataManager from './tef-data-manager.js'
import { loading_init, loading, ready_init, ready, toCSV, showUnknownVolcanoSamples } from './tef-utils.js'
import { getEventsURL, getVolcanoesURL, getSamplesURL } from './tef-urls.js'

// For the time now
Date.prototype.timeNow = function () {
  return ((this.getHours() < 10) ? "0" : "") + this.getHours() + ":" + ((this.getMinutes() < 10) ? "0" : "") + this.getMinutes() + ":" + ((this.getSeconds() < 10) ? "0" : "") + this.getSeconds();
}

export default function tef() {
  var tef = {}
  // INTERFACE COMPONENTS
  var _map
  var _timeline
  var _table
  var _scatterplot
  var _interactiveFilters

  // DATA RELATED VARIABLES
  var _samples
  var _dataManager

  // INITIALIZATION
  tef.init = function () {
    console.log((new Date()).timeNow())
    loading_init()
    Promise.all([
      d3.json(getVolcanoesURL()),
      d3.json(getEventsURL()),
      d3.json(getSamplesURL()), // ORI TO RETURN JSON
    ])
      .then(function (data_raw) {
        /*Papa.parse(getSamplesURL(), {
          download: true,
          header: true,
          delimiter: ";",
          complete: function (samples_raw, file) {
            _samples = samples_raw.data*/
        const volcanoes = data_raw[0].data
        const events = data_raw[1].data
        _samples = data_raw[2].data
        _samples.forEach(s => {
          // TODO: UGLY HACK
          if (s.Flag == "") {
            s.Flag = [' No_Flag']
          } else {
            const raw = s.Flag.split(', ')
            s.Flag = []
            raw.forEach(r => {
              s.Flag.push(r.trim())
            })
          }
          if (s.TypeOfRegister == "") {
            s.TypeOfRegister = "Not specified"
          }

          if (s.TypeOfAnalysis == "") {
            s.TypeOfAnalysis = "Not specified"
          }

          if (s.TypeOfSection == "") {
            s.TypeOfSection = "Not specified"
          }
        })
        _dataManager = dataManager().init(volcanoes, events, _samples)
        _interactiveFilters = interactiveFilters().init(tef, _dataManager.getCrossfilter())
        _map = map().init(tef, _dataManager.getCrossfilter()).addVolcanoes(_dataManager.getAllVolcanoes()).addSamplesInit(_dataManager.getAllVolcanoes())
        _timeline = timeline().init(tef).initTimeline(volcanoes, events)
        _table = table().init(tef)
        _scatterplot = scatterplot().init(
          tef,
          _dataManager.getAllVolcanoes(),
          _dataManager.getScatterplotCrossfilter(),
        )
        tef.addButtonListerners()
        ready_init()
        console.log((new Date()).timeNow())
        // }
        //})
      })
  }

  tef.addButtonListerners = function () {
    d3
      .select('#resetSelection')
      .on('click', () => {
        loading()
        setTimeout(() => {
          tef.resetSelection()
          dc.filterAll()
          dc.redrawAll()
          ready()
        }, 500)
      })
    d3
      .select('#downloadSelection')
      .on('click', () => {
        tef.downloadDataset(true)
      })
    d3
      .select('#downloadAllDataset')
      .on('click', () => {
        tef.downloadDataset(false)
      })
    d3
      .select('.scatter-controls')
      .selectAll('#scatterSwitch')
      .on('change', () => {
        loading()
        setTimeout(() => {
          _scatterplot.update()
          ready()
        })
      })
    d3
      .select('.tef-menu-filters-charts')
      .selectAll('#unknownSwitch')
      .on('change', () => {
        loading()
        setTimeout(() => {
          tef.toggleUnknownVolcanoSamples()
        }, 500)
      })
  }

  tef.resetSelection = function () {
    _dataManager.resetAllSelections()
    tef.update()
  }

  tef.downloadDataset = function (selections) {
    var postName = ''
    var rawData = ''
    if (selections) {
      rawData = _dataManager.getCrossfilter().allFiltered()
      postName = '_sample'
    } else {
      rawData = _dataManager.getCrossfilter().all()
    }
    const str = toCSV(rawData);
    const bytes = new TextEncoder().encode(str);
    const blob = new Blob([bytes], {
      type: "application/json;charset=utf-8"
    });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = url;
    a.download = 'BOOMDataset' + postName + '.csv';
    document.body.appendChild(a);
    a.click();
    window.URL.revokeObjectURL(url);
  }

  tef.toggleUnknownVolcanoSamples = function () {
    const showUnknownV = showUnknownVolcanoSamples()
    _dataManager.filter('unknownVolcano', showUnknownV)
    tef.update()
  }

  tef.highlighSampleInScatterplot = function (sample) {
    _dataManager.lastClickedSample(sample)
    _scatterplot.highlightSample()
  }

  tef.getLastClickedSample = function () {
    return _dataManager.lastClickedSample()
  }

  tef.update = function () {
    var isVolcanoSelectionActive = tef.isVolcanoSelectionActive()
    // TODO: check if I am considering when there is no selection
    const allVolcanoes = Object.keys(_dataManager.getAllVolcanoes());
    allVolcanoes.forEach(v => {
      const selectedEvents = _dataManager.getSelectedEventsPerVolcano(v)
      const allEvents = _dataManager.getAllEventsPerVolcano(v)
      const isVolcanoSelected = _dataManager.isVolcanoSelected(v)
      const volcanoColor = _dataManager.getVolcanoColor(v)
      const volcanoOrder = _dataManager.getVolcanoOrder(v)

      // TIMELINES
      _timeline.update(
        v,
        selectedEvents,
        volcanoColor,
        volcanoOrder,
        isVolcanoSelected,
        _dataManager.getInteractiveFilters()
      )

      // SOURCES TABLE
      _table.update(
        v,
        isVolcanoSelected,
        allEvents,
        selectedEvents,
        volcanoColor,
        volcanoOrder
      )

      // MAP
      /*const volcanoVisible = (isVolcanoSelectionActive && isVolcanoSelected) || (!isVolcanoSelectionActive)
      _map.update(v, volcanoVisible)*/

    })
    // SCATTERPLOT
    _scatterplot.update()
    dc.redrawAll()
    ready()
  }

  // VOLCANO SELECTION
  tef.filter = function (type, value) {
    loading()
    setTimeout(() => {
      _dataManager.filter(type, value)
      tef.update()
    })
  }

  tef.isVolcanoSelectionActive = function () {
    return _dataManager.isVolcanoSelectionActive()
  }

  tef.isVolcanoSelected = function (volcano) {
    return _dataManager.isVolcanoSelected(volcano)
  }

  tef.isEventSelected = function (event, volcano) {
    return _dataManager.isEventSelected(event, volcano)
  }

  return tef.init()
}

tef()