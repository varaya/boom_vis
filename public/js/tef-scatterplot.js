import { loading, ready, replaceAll, showOnlySelectedVolcanoes, showUnknownVolcanoSamples } from "./tef-utils.js"
import { getScatterplotRangeURL } from "./tef-urls.js"
import { unselectedColor } from './tef-colors.js'

export default function scatterplot() {
  var scatterplot = {}
  var _tef
  var _n
  var _elements
  var _hiddenOpacity = 0.1
  var _showUnknowVolcano = true
  var _scatterDimensions = []
  var _selectedVolcanes = {}
  var _ndx
  var _charts = []
  var _xDims = []
  var _yDims = []
  var _lastClickedSample = undefined

  // OBJECT INITIALIZATION
  scatterplot.init = function (tef, volcanes, ndx) {
    _tef = tef
    _n = 0
    _elements = getElementsList()
    _selectedVolcanes = volcanes
    _ndx = ndx

    const size = d3.select('#volcanoMap').node().getBoundingClientRect()
    const sizeTabs = d3.select('.nav-tabs').node().getBoundingClientRect()
    d3
      .select('#samplesChemistryTab')
      .style('height', size.height - sizeTabs.height)

    initScatterplot(_n)
    initButtons()
    return scatterplot
  }

  function initButtons() {
    d3.select('#addNewScatter')
      .on('click', () => {
        addNewScatter()
      })
  }

  function addNewScatter() {
    _n += 1
    initScatterplot(_n)
  }

  // DRAW THE SCATTERPLOTS  
  function initScatterplot(n) {
    const mainDiv = d3
      .select('#scatter')
      .append('div')
      .attr('id', 'scatter_' + n)
      .attr('class', 'col-6')
      .style('float', 'left')
      .style('padding', '10px')
      .style('width', '320px')
      .style('height', '300px')
    initElementSelectors(n)
    mainDiv.append('br')
    mainDiv.append('div').attr('id', 'scatterChart_' + n)
    mainDiv.append('div').attr('id', 'scatterChart_info_' + n)
    getDataScatterplot(n)
  }

  function getDataScatterplot(n) {
    var xDim = (d3.select('#xDim_' + n).selectAll('.active').node().id).substring(3)
    var yDim = (d3.select('#yDim_' + n).selectAll('.active').node().id).substring(3)
    _scatterDimensions[n] = _ndx
      .dimension((d) => { return [+d[xDim], +d[yDim], d['Volcano'], d['Event'], d['Flag']] })
      .filter(
        function (d) {
          return d[4] != 'Outlier'
        })
    drawNScatterplot(n)
  }

  function drawNScatterplot(n) {
    loading()
    const xDim = (d3.select('#xDim_' + n).selectAll('.active').node().id).substring(3)
    const yDim = (d3.select('#yDim_' + n).selectAll('.active').node().id).substring(3)
    const url = getScatterplotRangeURL(xDim, yDim)
    const unselColor = unselectedColor()
    d3
      .json(url)
      .then((data) => {
        var values = data.data[0]
        var xExtent = [values.min_x, values.max_x]
        var yExtent = [values.min_y, values.max_y]
        // TODO: UGLY HACK
        // This is done to add some extra space in the axes
        if (xDim != "87Sr_86Sr" && xDim != "143Nd_144Nd")
          xExtent = [xExtent[0] - xExtent[0] / 10, xExtent[1] + xExtent[1] / 10]
        if (yDim != "87Sr_86Sr" && yDim != "143Nd_144Nd")
          yExtent = [yExtent[0] - yExtent[0] / 10, yExtent[1] + yExtent[1] / 10]

        var isVolcanoSelectionActive = _tef.isVolcanoSelectionActive()

        const xDimLabel = cleanDimensionLabel(xDim)
        const xDimUnitLabel = getElementLabel(xDim)
        const yDimLabel = cleanDimensionLabel(yDim)
        const yDimUnitLabel = getElementLabel(yDim)

        _xDims[n] = xDim
        _yDims[n] = yDim

        var scatterGroup = _scatterDimensions[n].group()
        const newChart = new dc.ScatterPlot("#scatterChart_" + n)
          .height(300)
          .width(320)
          .useCanvas(true)
          .x(d3.scaleLinear().domain(xExtent))
          .y(d3.scaleLinear().domain(yExtent))
          .yAxisLabel(yDimLabel + xDimUnitLabel)
          .xAxisLabel(xDimLabel + yDimUnitLabel)
          .keyAccessor(function (d) { return d.key[0]; })
          .valueAccessor(function (d) { return d.key[1]; })
          .clipPadding(10)
          .dimension(_scatterDimensions[n])
          .highlightedSize(4)
          .symbolSize(5)
          .excludedOpacity(function (opacityKey) {
            // TODO: I DON'T UNDERSTAND THIS METHOD
            //return 0
            if (!isVolcanoSelectionActive) {
              return 0.05
            } else {
              if (_tef.isVolcanoSelected(opacityKey[0]) && _tef.isEventSelected(opacityKey[1], opacityKey[0]))
                return 0.05
              else
                return 0
            }
          })
          .excludedColor(unselColor)
          .group(scatterGroup)
          .colorAccessor(function (d) { return [d.key[2], d.key[3]]; })
          .colors(function (colorKey) {
            if (colorKey[0] in _selectedVolcanes)
              return _selectedVolcanes[colorKey[0]]['color']
            else
              return unselColor
          })
          .opacityAccessor(function (d) { return [d.key[2], d.key[3], d.key[0], d.key[1]]; })
          .opacity(function (opacityKey) { return getOpacity(opacityKey) })
          .emptySize(3)
          .emptyColor(unselColor)
          .title(function (d) { return d.value; })
        newChart.yAxis().ticks(5)
        newChart.xAxis().ticks(5)
        newChart.render();
        _charts[n] = newChart
        ready()
      })
  }

  function getOpacity(opacityKey) {
    //return 1
    if (!_showUnknowVolcano && opacityKey[0] == 'Unknown') return 0
    if (opacityKey[2] == '' || opacityKey[3] == '') return 0
    var ifSelectedVolcanoes = showOnlySelectedVolcanoes()
    if (!_tef.isVolcanoSelectionActive()) {
      if (ifSelectedVolcanoes) {
        return 0
      } else {
        return 1
      }
    } else {
      if (_tef.isVolcanoSelected(opacityKey[0]) && _tef.isEventSelected(opacityKey[1], opacityKey[0])) {
        return 1
      } else {
        return _hiddenOpacity
      }
    }
  }

  function initElementSelectors(n) {
    const dims = ['x', 'y']
    dims.forEach(thisDim => {
      const div = d3.selectAll('#scatter_' + n).append('div').attr('class', 'btn-group')
      const thisDimLabel = cleanDimensionLabel(thisDim)
      div
        .append('button')
        .attr('type', 'button')
        .attr('class', 'btn btn-secondary btn-sm dropdown-toggle')
        .attr('data-toggle', 'dropdown')
        .attr('aria-expanded', false)
        .attr('id', thisDim + 'Dim_' + n + '_label')
        .html(thisDimLabel)
      const divDrop = div.append('div')
        .attr('class', 'dropdown-menu')
        .attr('aria-labelledby', thisDim + 'Dim_' + n + '_label')
        .style('height', '400px')
        .style('overflow', 'scroll')
        .attr('id', thisDim + 'Dim_' + n)
      _elements.forEach((e, i) => {
        var active = false
        const eltoLabel = cleanDimensionLabel(e)
        if ((e == 'SiO2_normalized' && thisDim == 'x') || (e == 'K2O_normalized' && thisDim == 'y')) {
          active = true
        }
        divDrop
          .append('a')
          .attr('class', 'dropdown-item')
          .attr('id', 'el_' + e)
          .on('click', () => {
            d3
              .select('#' + thisDim + 'Dim_' + n)
              .selectAll('.dropdown-item')
              .classed('active', false)
            d3
              .select('#' + thisDim + 'Dim_' + n)
              .select('#el_' + e)
              .classed('active', true)
            const callback = () => { _tef.updateSelectedSamples() }
            getDataScatterplot(n, callback)
            const eltoActive = (div.select('#' + thisDim + 'Dim_' + n).selectAll('.active').node().id).substring(3)
            const eltoActiveLabel = cleanDimensionLabel(eltoActive)
            d3
              .selectAll('#' + thisDim + 'Dim_' + n + '_label')
              .html(thisDim + ' (' + eltoActiveLabel + ')')

          })
          .classed('active', active)
          .html(eltoLabel)
      })
      const eltoActive = (d3.select('#' + thisDim + 'Dim_' + n).selectAll('.active').node().id).substring(3)
      const eltoActiveLabel = cleanDimensionLabel(eltoActive)
      div
        .selectAll('#' + thisDim + 'Dim_' + n + '_label')
        .html(thisDim + ' (' + eltoActiveLabel + ')')
    })
  }

  var setHiddenOpacity = function () {
    const type = showOnlySelectedVolcanoes()
    if (!type) _hiddenOpacity = 0.05
    else _hiddenOpacity = 0
    return scatterplot
  }

  var setUnknownVisibility = function () {
    _showUnknowVolcano = showUnknownVolcanoSamples()
    return scatterplot
  }

  scatterplot.update = function () {
    setHiddenOpacity()
    setUnknownVisibility()
    drawLastClickedSample()
  }

  var getElementsList = function () {
    return [
      'SiO2_normalized',
      'TiO2_normalized',
      'Al2O3_normalized',
      'FeO_normalized',
      'Fe2O3_normalized',
      'Fe2O3T_normalized',
      'FeOT_normalized',
      'MnO_normalized',
      'MgO_normalized',
      'CaO_normalized',
      'Na2O_normalized',
      'K2O_normalized',
      'Cl_normalized',
      'Total_normalization',
      'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Cs',
      'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Sm', 'Eu', 'Gd',
      'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf',
      'Ta', 'Pb', 'Th', 'U',
      'La_Yb', 'Zr_Nb', '87Sr_86Sr',
      '143Nd_144Nd',
    ]
  }

  var getElementLabel = function (el) {
    const perc = [
      'SiO2_normalized',
      'TiO2_normalized',
      'Al2O3_normalized',
      'FeO_normalized',
      'Fe2O3_normalized',
      'Fe2O3T_normalized',
      'FeOT_normalized',
      'MnO_normalized',
      'MgO_normalized',
      'CaO_normalized',
      'Na2O_normalized',
      'K2O_normalized',
      'Cl_normalized',
      'Total_normalization']
    const ppm = ['Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Cs',
      'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Sm', 'Eu', 'Gd',
      'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf',
      'Ta', 'Pb', 'Th', 'U']
    var index = perc.indexOf(el)
    if (index >= 0) return ' (wt.%)'
    index = ppm.indexOf(el)
    if (index >= 0) return ' (ppm)'
    return ''
  }

  var cleanDimensionLabel = function (dim) {
    var result = dim
    result = replaceAll(result, '_normalized', ' norm.')
    result = replaceAll(result, '_normalization', ' norm.')
    result = replaceAll(result, '_', '/')
    return result
  }

  scatterplot.highlightSample = function () {
    drawLastClickedSample()
  }

  function drawLastClickedSample() {
    dc.redrawAll()
    const lastClickedSample = _tef.getLastClickedSample()
    if(lastClickedSample === undefined) return
    loading()
    setTimeout(function () {
      for (let index in _charts) {
        var thereWasValues = false
        lastClickedSample.forEach(sample => {
          let thisChart = _charts[index]
          let canvas = d3.select('#scatterChart_' + index).select('canvas').node();
          let context = canvas.getContext('2d');
          let xDim = _xDims[index]
          let yDim = _yDims[index]

          if (sample[xDim] != "" && sample[yDim] != "") {
            thereWasValues = true
            let centerX = thisChart.x()(sample[xDim])
            let centerY = thisChart.y()(sample[yDim])
            var radius = 4;
            context.beginPath();
            context.arc(centerX, centerY, radius, 0, 2 * Math.PI, true);
            context.lineWidth = 4;
            context.strokeStyle = '#000';
            context.fillStyle = '#fff';
            context.stroke();
            context.fill();
          }
          // This is just to test the highlighted samples
          /*_allDimensions.filter(null)
          _allDimensions.filterFunction(d => {
            if(d["ID"] == sample.OriginalID) console.log(d)
            return d["ID"] == sample.OriginalID 
          })*/
        })
        if (!thereWasValues) {
          d3
            .select('#scatterChart_info_' + index)
            .html("No data")
        } else {
          d3
            .select('#scatterChart_info_' + index)
            .html("")
        }
      }
      ready()
    }, 100)
  }
  return scatterplot
}
