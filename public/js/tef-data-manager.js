
export default function dataManager() {
    var manager = {}

    // VOLCANOES
    var _nSelectedVolcanoes
    var _volcanoesSelection // selection per volcano
    var _volcanoes

    // INTERACTIVE FILTERS
    var _interactiveFilters

    // UNKNOWN VOLCANO FILTER
    var _showUnknowVolcano

    var _ndx
    var _ndxAllDim
    var _ndxScatter
    var _ndxScatterAllDim

    var _lastClickedSample

    manager.init = function (volcanoes, events, dataRaw) {
        // BASIC
        _ndx = crossfilter(dataRaw)
        _ndxAllDim = _ndx.dimension(d => d)

        // Scatter should only be able to be filtered by
        // the interactive filters
        _ndxScatter = crossfilter(dataRaw)
        _ndxScatterAllDim = _ndxScatter.dimension(d => d)

        // VOLCANOES
        _volcanoesSelection = {}
        _volcanoes = {}
        _nSelectedVolcanoes = 0
        volcanoes.forEach((v, index) => {
            _volcanoes[v.Name] = {}
            _volcanoes[v.Name].events = {}
            _volcanoes[v.Name].color = v.Color
            _volcanoes[v.Name].order = index;
            _volcanoes[v.Name].latitude = v.Latitude;
            _volcanoes[v.Name].longitude = v.Longitude;
            // TODO: This is repeated info
            _volcanoes[v.Name].name = v.Name;

            _volcanoesSelection[v.Name] = {}
            _volcanoesSelection[v.Name].isSelected = false
            _volcanoesSelection[v.Name].events = []
        })
        for (let index in events) {
            let e = events[index]
            let v = e.Volcano
            if (!(v in _volcanoes)) continue
            _volcanoes[v].events[e.Name] = e
        }

        // INTERACTIVE FILTERS
        _interactiveFilters = {}
        _interactiveFilters['TypeOfRegister'] = []
        _interactiveFilters['TypeOfAnalysis'] = []
        _interactiveFilters['TypeOfSection'] = []
        _interactiveFilters['Flag'] = []

        // UNKNOWN VOLCANO FILTER
        _showUnknowVolcano = true

        // STORE LAST CLICKED SAMPLE
        _lastClickedSample = undefined

        return manager
    }

    ////////////////////////////
    // SELECTION AND FILTER 
    ////////////////////////////
    manager.filter = function (type, value) {
        if (type == 'volcano') {
            filterVolcano(value)
        } else if (type == 'event') {
            filterEvent(value.volcano, value.event)
        } else if (type == 'unknownVolcano') {
            filterUnknownVolcano(value)
        } else {
            _interactiveFilters[type] = value
            updateScatterCrossfilter()

        }
    }

    var updateCrossfilter = function () {
        const selectedVolcanoes = manager.getSelectedVolcanoes()
        if (selectedVolcanoes.length == 0) {
            if (_showUnknowVolcano)
                _ndxAllDim.filterFunction(d => d)
            else
                _ndxAllDim.filterFunction(d => d.Volcano != 'Unknown')
        } else {
            _ndxAllDim.filterFunction(d => {
                if (_showUnknowVolcano)
                    return selectedVolcanoes.indexOf(d.Volcano) >= 0 && _volcanoesSelection[d.Volcano].events.indexOf(d.Event) >= 0
                else
                    return d.Volcano != 'Unknown' && selectedVolcanoes.indexOf(d.Volcano) >= 0 && _volcanoesSelection[d.Volcano].events.indexOf(d.Event) >= 0
            })
        }
    }

    var updateScatterCrossfilter = function () {
        _ndxScatterAllDim.filter(null)
        _ndxScatterAllDim.filterFunction(d => {
            var flag = true
            for (var iaf in _interactiveFilters) {
                if (_interactiveFilters[iaf].length > 0) {
                    // FLAG are lists
                    if (iaf == "Flag") {
                        var atLeatOne = false
                        _interactiveFilters[iaf].forEach(f => {
                            console.log(f)
                            console.log(d[iaf])
                            if (d[iaf].includes(f)) {
                                //console.log('THERE IS!')
                                atLeatOne = true
                            }
                        })
                        console.log(atLeatOne)
                        if (atLeatOne == false) {flag = false}
                    } else {
                        if (_interactiveFilters[iaf].indexOf(d[iaf]) < 0) {
                            flag = false
                        }
                    }
                }
            }
            return flag
        })
    }

    var filterVolcano = function (v) {
        toggleVolcano(v)
        updateCrossfilter()
    }

    var toggleVolcano = function (volcano) {
        _volcanoesSelection[volcano].isSelected = !_volcanoesSelection[volcano].isSelected
        const isSelected = _volcanoesSelection[volcano].isSelected
        if (isSelected) {
            _volcanoesSelection[volcano].events = [...Object.keys(_volcanoes[volcano].events)]
            _nSelectedVolcanoes += 1
        } else {
            _volcanoesSelection[volcano].events = []
            _nSelectedVolcanoes -= 1
        }
        return manager
    }

    var filterEvent = function (volcano, event) {
        toggleEvent(volcano, event)
        updateCrossfilter()

    }

    var toggleEvent = function (volcano, event) {
        // 1) WAS THE VOLCANO SELECTED?
        const isVolcanoSelected = _volcanoesSelection[volcano].isSelected
        // IF VOLCANO SELECTED, PROCESS THE EVENTS
        if (isVolcanoSelected) {
            // 2) WAS THE EVENT ALREADY SELECTED?
            // IF EVENT WAS ALREADY SELECTED, UNSELECTED
            const indexEvent = _volcanoesSelection[volcano].events.indexOf(event)
            if (indexEvent >= 0) {
                // 3) REMOVE THE EVENT
                _volcanoesSelection[volcano].events.splice(indexEvent, 1);

                // 4) IF THERE IS NO EVENT SELECTED, UNSELECT THE VOLCANO
                if (_volcanoesSelection[volcano].events.length == 0) {
                    _volcanoesSelection[volcano].isSelected = false
                    _nSelectedVolcanoes -= 1
                }
            }
            // IF EVENT WAS NOT SELECTED, SELECT IT
            else {
                _volcanoesSelection[volcano].events.push(event)
            }
            // IF NOT, ACTIVATE IT AND ADD THE EVENT AS SELECTED
        } else {
            _volcanoesSelection[volcano].isSelected = true
            _volcanoesSelection[volcano].events = [event]
            _nSelectedVolcanoes += 1
        }
    }

    var filterUnknownVolcano = function (showUnknowVolcano) {
        _showUnknowVolcano = showUnknowVolcano
        updateCrossfilter()
    }
    manager.isVolcanoSelectionActive = function () {
        return _nSelectedVolcanoes > 0
    }

    manager.resetAllSelections = function () {
        _ndxAllDim.filter(null)
        _ndxScatterAllDim.filter(null)
        for (var v in _volcanoesSelection) {
            _volcanoesSelection[v].isSelected = false
            _volcanoesSelection[v].events = []
        }
        _nSelectedVolcanoes = 0
        _lastClickedSample = undefined
    }

    ////////////////////////////
    // GETTERS ALL   
    ////////////////////////////
    manager.getCrossfilter = function () {
        return _ndx
    }

    manager.getScatterplotCrossfilter = function () {
        return _ndxScatter
    }

    manager.getAllVolcanoes = function () {
        return _volcanoes
    }

    manager.getSelectedVolcanoes = function () {
        var r = []
        for (var v in _volcanoesSelection) {
            if (_volcanoesSelection[v].isSelected) {
                r.push(v)
            }
        }
        return r
    }

    manager.getInteractiveFilters = function () {
        return _interactiveFilters
    }

    manager.lastClickedSample = function (arg) {
        if (arg === undefined) return _lastClickedSample
        _lastClickedSample = arg
    }

    ////////////////////////////
    // GETTERS PER VOLCANO   
    ////////////////////////////
    manager.getVolcanoColor = function (v) {
        if (v in _volcanoes) {
            return _volcanoes[v].color
        }
        return '#000'
    }

    manager.getVolcanoOrder = function (v) {
        if (v in _volcanoes) {
            return _volcanoes[v].order
        }
        return -1
    }

    manager.isVolcanoSelected = function (volcano) {
        if (volcano in _volcanoesSelection) {
            return _volcanoesSelection[volcano].isSelected
        }
        return false
    }

    manager.isEventSelected = function (event, volcano) {
        if (volcano in _volcanoesSelection && _volcanoesSelection[volcano].isSelected) {
            return _volcanoesSelection[volcano].events.indexOf(event) >= 0
        }
        return false
    }

    manager.getSelectedEventsPerVolcano = function (volcano) {
        return _volcanoesSelection[volcano].events
    }

    manager.getAllEventsPerVolcano = function (volcano) {
        return _volcanoes[volcano].events
    }
    return manager

}