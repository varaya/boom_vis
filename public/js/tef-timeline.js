import { loading, ready, cleanName } from './tef-utils.js'
import { tooltipMouseOver, tooltipMouseOut } from './tef-interactions.js'
import { getEventsURL, getSamplesURL } from './tef-urls.js'
import { sampleClick } from './tef-interactions.js'
import { sampleInfo, sampleObservationInfo } from './tef-info-content.js'

export default function timeline() {
  var timeline = {}
  var _ctx
  var _ctxFocus
  var _minAge14C
  var _maxAge14C
  var _tef
  var _xFocusScale
  var _yFocusScale

  ////////////////////////////
  // INITIALIZE 
  ////////////////////////////
  timeline.init = function (tef) {
    _tef = tef
    _ctx = {
      w: 600,
      h: 850,
      vmargin: 2,
      hmargin: 10,
      timeParser: d3.timeParse('%Y-%m-%d'),
      timeAxisHeight: 10,
    }
    _ctxFocus = {
      w: 0,
      h: 850,
      vmargin: 40,
      hmargin: 99,
      timeAxisHeight: 15,
    }
    _minAge14C = -15000
    _maxAge14C = 0
    return timeline
  }


  ////////////////////////////
  // CREATION OF D3 OBJECTS
  ////////////////////////////

  timeline.initTimeline = function (volcanoes, events) {

    const size = d3.select('#volcanoMap').node().getBoundingClientRect()
    const sizeTabs = d3.select('.nav-tabs').node().getBoundingClientRect()
    _ctx.w = size.width - _ctx.hmargin
    _ctx.h = size.height - sizeTabs.height - 20
    _ctxFocus.h = size.height - sizeTabs.height - 70

    const left = (_ctx.w - 150) / 2
    d3
      .select('#timeline')
      .append('div')
      .style('float', 'left')
      .style('display', 'inline-block')
      .style('font-size', '10px')
      .style('color', '#212529')
      .style('position', 'relative')
      .style('left', left + 'px')
      .html('14'.sup() + 'C years BP')

    // CREATE SVG CONTAINER
    var svg = d3.select('#timeline').append('svg')
    svg.attr('width', _ctx.w)
    svg.attr('height', _ctx.h + 20)
    svg.style('margin-top', 10)
    svg.style('margin-left', _ctx.hmargin / 2)

    // ADD TOOLTIP
    const div = d3.select('body')
      .append('div')
      .attr('class', 'tef-tooltip leaflet-container')
      .style('opacity', 0)
      .append('div')
      .attr('class', 'leaflet-popup leaflet-zoom-animated')
    div.append('div')
      .attr('class', 'leaflet-popup-content-wrapper')
      .append('div')
      .attr('class', 'leaflet-popup-content')
    /*div
      .append('a')
      .attr('class', 'leaflet-popup-close-button')
      .on('click', () => {
        tooltipClose()
      })
      .html('x')*/

    const BAND_H = (_ctx.h - _ctx.timeAxisHeight) / volcanoes.length

    // ADD X SCALE
    var xScale = d3.scaleLinear()
      .domain([-15000, 0])
      .range([0, _ctx.w - 150])

    var xAxis = d3.axisTop()
      .scale(xScale)
      .tickFormat((d) => Math.abs(d))

    svg
      .append('g')
      .attr('transform', 'translate(' + _ctx.hmargin + ',25)')
      .call(xAxis)

    // ADD VOLCANO CONTAINER
    var volcanosGroup = svg
      .append('g')
      .attr('transform', 'translate(0, 25)')
      .selectAll('.timeContainer')
      .data(volcanoes)
      .enter()
      .append('g')
      .attr('class', 'timeContainer')
      .attr('transform', function (d, i) {
        return (
          'translate(' +
          _ctx.hmargin +
          ',' +
          (i * BAND_H + 1 + _ctx.timeAxisHeight + 5) +
          ')'
        )
      })
      .attr('id', function (d) {
        return cleanName(d.Name)
      })

    volcanosGroup
      .append('line')
      .attr('class', 'line-container')
      .attr('x1', 0)
      .attr('y1', _ctx.timeAxisHeight)
      .attr('x2', _ctx.w - 150)
      .attr('y2', _ctx.timeAxisHeight)
      .style('stroke', 'gray')
      .style('fill', 'none')

    volcanosGroup
      .append('text')
      .style('font-size', '10px')
      .attr('x', _ctx.w - 150 + 10)
      .attr('y', _ctx.timeAxisHeight / 1.3)
      .text(function (d) {
        return d.Name
      })
      .on('mouseenter', function (e, d, i) {
        d3.select(this).style("cursor", "pointer");
      })
      .on('mouseout', function () {
        d3.select(this).style("cursor", "default");
      })
      .on('click', (e, d) => {
        _tef.filter('volcano', d.Name)
      })

    // ADD EVENTOS RECTANGLES
    var timeIntervals = volcanosGroup
      .selectAll('.timeRect')
      .data(function (d) {
        var events_volcan = events.filter(
          (e) =>
            e.Volcano == d.Name &&
            e.MinAge14C &&
            e.MaxAge14C &&
            e.Name !== 'Unknown',
        )
        events_volcan.forEach((e) => (e.Color = d.Color))
        return events_volcan
      })
      .enter()
    timeIntervals
      .append('rect')
      .attr('class', 'rectevento')
      .attr('x', function (d) {
        return xScale(d.MinAge14C)
      })
      .attr('y', 0)
      .attr('width', function (d) {
        return xScale(d.MaxAge14C) - xScale(d.MinAge14C)
      })
      .attr('height', _ctx.timeAxisHeight)
      .style('fill', '#525252')
      .attr('opacity', 0.5)
      .on('mouseenter', function (e, d, i) {
        d3.select(this).style("cursor", "pointer");
        tooltipMouseOver('event', d, e)
      })
      .on('mouseout', function () {
        d3.select(this).style("cursor", "default");
        tooltipMouseOut()
      })
      .on('click', (e, d) => { _tef.filter('event', { 'volcano': d.Volcano, 'event': d.Name }) })
    timeline.initFocusTimeline()
    return timeline
  }

  timeline.initFocusTimeline = function () {
    var svg = d3.select('#focus-timeline').append('svg')
    svg.attr('width', _ctxFocus.w + _ctxFocus.hmargin)
    svg.attr('height', _ctxFocus.h + _ctxFocus.vmargin)
    const main_g = svg.append('g')
    main_g.attr(
      'transform',
      `translate(1, ${_ctxFocus.vmargin})`,
    )

    _yFocusScale = d3.scaleLinear()
      .domain([_minAge14C - 200, _maxAge14C + 200])
      .range([_ctxFocus.h - _ctxFocus.vmargin, 0])

    _xFocusScale = d3.scalePow().exponent(5).domain([1, 6]).range([40, 100])
    let l = ' 14C AP'
    var yAxis = d3.axisLeft()
      .scale(_yFocusScale)
      .tickFormat((d) => Math.abs(d) + l)
    d3
      .select('#focus-timeline-axis')
      .append('svg')
      .attr('width', 100)
      .attr('height', _ctxFocus.h)
      .append('g')
      .attr('transform', `translate(${_ctxFocus.hmargin}, ${_ctxFocus.vmargin})`)
      .call(yAxis)
    main_g.append('g').attr('id', 'events')
  }

  ////////////////////////////
  // UPDATE 
  ////////////////////////////
  timeline.update = function (volcano, events, volcanoColor, volcanoOrder, isVolcanoSelected, interactiveFilters) {
    var daDiv = document.getElementById('focus-timeline');
    daDiv.scrollLeft = 0;

    const name = cleanName(volcano)
    const volcanoG = d3.selectAll('#timeline')
      .selectAll('svg')
      .selectAll('#' + name)

    if (isVolcanoSelected) {
      const textGSize = volcanoG.select('text').node().getBoundingClientRect()
      volcanoG
        .append('rect')
        .attr('class', 'volcanoSelectedRect')
        .attr('x', _ctx.w - 150 + 10 - 2)
        .attr('y', -4)
        .attr('width', textGSize.width + 4)
        .attr('height', textGSize.height + 2)
        .style('fill', 'none')
        .style('stroke', d => d.Color)
        .lower()
      volcanoG
        .select('.line-container')
        .style('stroke', d => d.Color)
        .style('stroke-width', 2)
      d3
        .selectAll('.rectevento')
        .filter((d) => d.Volcano == volcano)
        .style('fill', (d) => {
          if (d.Volcano == volcano && events.indexOf(d.Name) >= 0) {
            return d.Color
          } else {
            return '#525252'
          }
        })

      // DON'T ADD ZOOM TIMELINE IT AGAIN!
      if (d3.select('#focus-timeline').selectAll('#events').selectAll('#focus_g_' + name).empty()) {
        addEvents(volcano, volcanoColor, volcanoOrder, interactiveFilters)
      }

      // TODO: When it's the same volcano, the async behavior
      // of javascript does not cover it. It should not be a problem
      // as I use the interactive filters as parameter when I add the 
      // components of the zoomed timeline
      updateZoomtimeLine(volcano, interactiveFilters)

    } else {
      volcanoG
        .selectAll('.volcanoSelectedRect')
        .remove()
      volcanoG
        .select('.line-container')
        .style('stroke', 'gray')
        .style('stroke-width', 1)
      d3.selectAll('.rectevento')
        .filter((d) => d.Volcano == volcano)
        .style('fill', '#525252')
      removeEvents(volcano)
    }
    reOrderVolcanoes()

  }

  var addEvents = function (volcanoName, color, order, interactiveFilters) {
    _ctxFocus.w = _ctxFocus.w + 100
    d3.select('#focus-timeline').select('svg').attr('width', _ctxFocus.w + _ctxFocus.hmargin)

    loading()
    Promise.all([
      d3.json(getEventsURL(volcanoName)),
      d3.json(getSamplesURL(volcanoName))
    ])
      .then(data => {
        ready()
        const events = data[0].data
        const muestras = data[1].data

        ////////////////////
        // 1) DATA WRANGLING 
        var dataEvents = {}
        events.forEach(e => {
          if (e.Name == 'Unknown') {
            return
          }
          const tmp = {}
          tmp.Magnitude = e.Magnitude
          tmp.MinAge14C = e.MinAge14C
          tmp.MaxAge14C = e.MaxAge14C
          tmp.Vei = e.Vei
          tmp.Volcano = e.Volcano
          tmp.Name = e.Name
          tmp.Samples = []
          tmp.Color = color
          dataEvents[e.Name] = tmp
        })

        muestras.forEach(m => {
          if (m.Event == 'Unknown' || m['14C_Age'] == '') {
            return
          }
          // TODO: UGLY HACK WHY THIS?
          if (m.Flag == "") m.Flag = ' No_Flag'
          dataEvents[m.Event].Samples.push(m)
        })

        var svg = d3.select('#focus-timeline').selectAll('svg').selectAll('#events')
        var n = svg.selectAll('.volcan-focus').size()



        //////////////////////////////////
        // 2) CREATE GROUP OBJECT PER VOLCANO
        const volcanoNameClean = cleanName(volcanoName)
        const eventoG = svg
          .selectAll('.nothing')
          .data([{ 'name': volcanoName, 'order': order }])
          .enter()
          .append('g')
          .attr('class', 'volcan-focus')
          .attr('id', 'focus_g_' + volcanoNameClean)
          .attr('order', order)
          .attr('transform', 'translate(' + n * 100 + ', 0)')

        eventoG
          .append('rect')
          .attr('width', 100)
          .attr('height', _ctxFocus.h - _ctxFocus.vmargin)
          .style('stroke', 'black')
          .style('fill', 'none')

        var label = volcanoName.substring(0, 10)
        if (volcanoName.length > 10) label += '...'
        eventoG
          .append('text')
          .text(label)
          .attr('transform', 'translate(0, -10)')
          .on('mouseover', function (e, d) {
            d3.select(this).style("cursor", "pointer");
            tooltipMouseOver('label', volcanoName, e)
          })
          .on('mouseout', function () {
            d3.select(this).style("cursor", "default");
            tooltipMouseOut()
          })
        const eventsGroups = eventoG.append('g').attr('id', 'focus_events_' + volcanoNameClean)

        ///////////////////////
        // 3) ADD EACH EVENT (RECTANGLE)
        var timeIntervals = eventsGroups
          .selectAll('.nothing')
          .data(() => {
            var tmp = []
            for (var eventName in dataEvents)
              tmp.push(dataEvents[eventName])
            return tmp
          })
          .enter()
          .append('g')
          .attr('class', (d) => {
            const eventNameClean = cleanName(d.Name)
            return (
              'timeRectFocus timeRectFocus_' +
              volcanoNameClean +
              '_' +
              eventNameClean
            )
          })

        timeIntervals
          .append('rect')
          .attr('class', 'rectevento')
          .attr('y', function (d) {
            return _yFocusScale(d.MaxAge14C)
          })
          .attr('x', (d) => {
            return 0
          })
          .attr('height', function (d) {
            return _yFocusScale(d.MinAge14C) - _yFocusScale(d.MaxAge14C)
          })
          .attr('width', (d) => {
            if (d.Vei == -1 || d.Vei == '' || d.Vei == undefined) {
              return 20
            } else {
              return _xFocusScale(d.Vei)
            }
          })
          .style('fill', color)
          .style('opacity', 0.3)
          .on('mouseover', function (e, d) {
            d3.select(this).style("cursor", "pointer");
            tooltipMouseOver('event', d, e)
          })
          .on('mouseout', function () {
            d3.select(this).style("cursor", "default");
            tooltipMouseOut()
          })
          .on('click', (e, d) => {
            _tef.filter('event', { 'volcano': d.Volcano, 'event': d.Event })
          })

        ////////////////////////
        // 4) ADD SAMPLES (LINES) PER EVENT
        timeIntervals
          .selectAll('.focus-interval')
          .data((d) => d.Samples)
          .enter()
          .append('line')
          .attr('y1', function (d) {
            return _yFocusScale(d['14C_Age_Num'])
          })
          .attr('y2', function (d) {
            return _yFocusScale(d['14C_Age_Num'])
          })
          .attr('x1', 0)
          .attr('x2', (d) => {
            if (d.Vei == -1 || d.Vei == '' || d.Vei == undefined) {
              return 20
            } else {
              return _xFocusScale(d.Vei)
            }
          })
          .style('stroke', color)
          .style('stroke-width', 2)
          .style('visibility', d => sampleVisibility(d, interactiveFilters))
          .on('mouseover', function (e, d) {
            d3.select(this).style("cursor", "pointer");
            tooltipMouseOver('sampleObservation', d, e)
          })
          .on('mouseout', function () {
            d3.select(this).style("cursor", "default");
            tooltipMouseOut()
          })
          .on('click', (d, e) => {
            sampleClick(sampleObservationInfo(e))
          })


        ////////////////////////
        // 5) ADD SAMPLES UNKNOWN
        timeIntervals
          .selectAll('.unknown-muestras')
          .data((d) => {
            const tmp = []
            muestras.forEach((m) => {
              if (m.Event == 'Unknown') {
                if (m['14C_Age'] != '' && !isNaN(m['14C_Age_Num']) && m['14C_Age_Num'] != '') {
                  tmp.push(m)
                }
              }
            })
            return tmp
          })
          .enter()
          .append('line')
          .attr('y1', function (d) {
            return _yFocusScale(d['14C_Age_Num'])
          })
          .attr('y2', function (d) {
            return _yFocusScale(d['14C_Age_Num'])
          })
          .attr('x1', 0)
          .attr('x2', (d) => {
            if (d.Vei == -1 || d.Vei == '' || d.Vei == undefined) {
              return 20
            } else {
              return _xFocusScale(d.Vei)
            }
          })
          .style('stroke', '#7e7e7e')
          .style('stroke-width', 2)
          .style('visibility', d => sampleVisibility(d, interactiveFilters))
          .on('mouseover', function (e, d) {
            d3.select(this).style("cursor", "pointer");
            tooltipMouseOver('sampleObservation', d, e)
          })
          .on('mouseout', function () {
            d3.select(this).style("cursor", "default");
            tooltipMouseOut()
          })
          .on('click', (d, e) => {
            sampleClick(sampleObservationInfo(e))
          })
        reOrderVolcanoes()
      })
  }

  var removeEvents = function (volcan) {
    const volcanoName = cleanName(volcan)
    d3.selectAll('#focus_g_' + volcanoName).remove()
  }

  var updateZoomtimeLine = function (volcanoName, interactiveFilters) {
    const volcanoNameClean = cleanName(volcanoName)
    const eventsGroups = d3.selectAll('#focus_events_' + volcanoNameClean)
    eventsGroups
      .selectAll('.timeRectFocus')
      .selectAll('line')
      .style('visibility', d => sampleVisibility(d, interactiveFilters))
  }

  var sampleVisibility = function (d, interactiveFilters) {
    var visible = true
    for (var t in interactiveFilters) {
      const activeFilters = interactiveFilters[t]
      if (activeFilters.length > 0) {
        if (t == 'Flag') {
          const allFlags = d.Flag.split(', ')
          allFlags.forEach(f => {
            if (activeFilters.indexOf(f.trim())) {
              visible = false
            }
          })
        } else {
          if (activeFilters.indexOf(d[t]) < 0) {
            visible = false
          }
        }
      }
    }
    if (visible) {
      return 'visible'
    } else {
      return 'hidden'
    }
  }

  var reOrderVolcanoes = function () {
    const orderedGroups = d3
      .select('#focus-timeline')
      .selectAll('#events')
      .selectAll('.volcan-focus')
      .sort(function (a, b) {
        return a.order - b.order;
      });
    orderedGroups.each(function (g, index) {
      d3
        .select(this)
        .attr('transform', 'translate(' + index * 100 + ', 0)')
    })
  }

  return timeline
}
