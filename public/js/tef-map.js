import { sampleInfo } from './tef-info-content.js'
import { sampleClick } from './tef-interactions.js'

export default function map() {
  var map = {}
  var _mapContainer
  var _volcanoes
  var _volcanoesLayer
  var _ndx
  var _tef

  // INITIALIZATION
  map.init = function (tef, ndx) {
    _tef = tef
    _ndx = ndx

    // SETUP MAP LAYERS
    // In case of change: https://leaflet-extras.github.io/leaflet-providers/preview/

    const grayscale = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ',
    });

    const satellite = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
    });

    // CONTROL LAT LON
    L.Control.LatLonLabel = L.Control.extend({
      onAdd: function (map) {
        var div = L.DomUtil.create('div', 'info legend');
        div.innerHTML = '';
        return div;
      },

      onRemove: function (map) {
        // Nothing to do here
      }
    });
    L.control.latLongLabel = function (opts) {
      return new L.Control.LatLonLabel(opts);
    }

    const latLonLabel = L.control.latLongLabel({ position: 'bottomright' })

    L.CursorHandler = L.Handler.extend({

      addHooks: function () {
        this._map.on('mouseover', this._open, this);
        this._map.on('mousemove', this._update, this);
        this._map.on('mouseout', this._close, this);
      },

      removeHooks: function () {
        this._map.off('mouseover', this._open, this);
        this._map.off('mousemove', this._update, this);
        this._map.off('mouseout', this._close, this);
      },

      _open: function (e) {
        this._update(e);
      },

      _close: function () {
        this._map.closePopup(this._popup);
      },

      _update: function (e) {
        latLonLabel.getContainer().innerHTML = "Lat: " + e.latlng.lat.toFixed(3) + ", lon: " + e.latlng.lng.toFixed(3);
      }


    });

    L.Map.addInitHook('addHandler', 'cursor', L.CursorHandler);


    // CREATE MAP OBJECT
    _mapContainer = L.map('volcanoMap', { cursor: true }).setView([-45, -71.489618], 6)

    grayscale.addTo(_mapContainer)

    // ADD CONTROLS
    L.control.scale().addTo(_mapContainer)
    var baseMaps = {
      "Grayscale": grayscale,
      "Satellite": satellite
    };
    L.control.layers(baseMaps).addTo(_mapContainer);
    latLonLabel.addTo(_mapContainer)

    return map
  }
  map.addVolcanoes = function (volcanoes) {
    _volcanoes = {}
    for (var volcanoName in volcanoes) {
      var volcano = volcanoes[volcanoName]
      var lat = Number(volcano.latitude)
      var lon = Number(volcano.longitude)
      var volcanTip = '<div>'
      volcanTip += '<h4>' + volcanoName + '</h4>'
      volcanTip += '<h4>(' + [lat, lon] + ')</h4>'
      volcanTip += '</div>'
      var latLng = L.latLng(Number(volcano.latitude), Number(volcano.longitude))
      var volcanIcon = L.triangleMarker(latLng, {
        rotation: 0,
        width: 20,
        height: 20,
        color: volcano.color,
        fillColor: '#525252'
      })
      volcanIcon.id = volcanoName
      volcanIcon.color = volcano.color
      volcanIcon.isVisible = true
      volcanIcon
        .addTo(_mapContainer)
        .bindPopup(volcanTip, {
          autoPan: false,
        })
        .on('click', function (e) {
          var id = e.target.id
          _tef.filter('volcano', id)
        })
      volcanIcon.on('mouseover', function (e) {
        this.openPopup()
      })
      volcanIcon.on('mouseout', function (e) {
        this.closePopup()
      })
      _volcanoes[volcanoName] = volcanIcon
    }
    return map
  }

  map.addSamplesInit = function (volcanoes) {

    function reduceAdd(p, v, nf) {
      ++p.Count;
      // TODO: this is kinda dirty
      // THIS CAN BE FIXED IF VARIABLES START WITH LOWERCASE
      p.Object.push(v)
      if (v.Volcano in volcanoes)
        p.Color = volcanoes[v.Volcano].color
      p.OffsetLat = Math.random() * 0.001;
      p.OffsetLng = Math.random() * 0.001;
      return p;
    }

    function reduceRemove(p, v, nf) {
      --p.Count;
      return p;
    }

    function reduceInitial() {
      var r = {}
      r.Count = 0
      r.Object = []
      r.Color = ''
      r.OffsetLat = 0
      r.OffsetLng = 0
      return r;
    }

    var samples = _ndx.dimension(d => [d.SampleID, d.Volcano, d.Latitude]);
    var samplesGroup = samples.group().reduce(reduceAdd, reduceRemove, reduceInitial)

    var marker = dc_leaflet.markerCircleChart("#volcanoMap")
      .dimension(samples)
      .group(samplesGroup)
      .map(_mapContainer)
      .showMarkerTitle(true)
      .locationAccessor(d => { return (Number(d.value.Object[0].Latitude) + d.value.OffsetLat) + ',' + (Number(d.value.Object[0].Longitude) + d.value.OffsetLng) })
      .valueAccessor(d => d.value.Count)
      .fitOnRender(true)
      .fitOnRedraw(true)
      .filterByArea(false)
      .cluster(false)
      .r(d => 5)
      .popup(d => {
        return sampleInfo(d.value.Object, d.value.Count)
      })
      .clickEvent((d, e) => {
        sampleClick(d.sourceTarget._popup._content)
        _tef.highlighSampleInScatterplot(d.sourceTarget.options.object)
      })
    marker.render()

    return map
  }

  map.update = function (volcanoName, isVisible) {
    const icon = _volcanoes[volcanoName]
    if (isVisible) {
      if (!icon.isVisible) {
        icon.addTo(_mapContainer)
      }
    } else {
      if (icon.isVisible) {
        _mapContainer.removeLayer(icon)
      }
    }
    icon.isVisible = isVisible
  }

  return map
}
