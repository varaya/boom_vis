import { hexToRGBString, loading, ready, cleanName } from './tef-utils.js'
import { unselectedColor } from './tef-colors.js'
import { getAnalysisByAuthorsURL } from './tef-urls.js'

export default function table() {
  var table = {}
  var _tef

  // INITIALIZATION
  table.init = function (tef) {
    _tef = tef
    return table
  }

  table.update = function (volcanoName, isSelected, volcanoAllEvents, volcanoSelectedEvents, volcanoColor, volcanoOrder) {
    const volcanoNameClean = cleanName(volcanoName)
    if (isSelected) {
      // DON'T ADD IT AGAIN!
      if (d3.selectAll('#summary_' + volcanoNameClean).empty()) {
        addVolcanoSummary(volcanoName, volcanoAllEvents, volcanoSelectedEvents, volcanoColor, volcanoOrder)
      }
      updateEvents(volcanoName, volcanoSelectedEvents, volcanoColor)
    } else {
      removeVolcanoSummary(volcanoName)
    }
    reOrderVolcanoes()
  }

  var updateEvents = function (volcanoName, volcanoSelectedEvents, volcanoColor) {
    const volcanoNameClean = cleanName(volcanoName)
    var unselColor = unselectedColor()
    d3
      .select('#sourcesPerVolcano')
      .selectAll('#summary_' + volcanoNameClean)
      .selectAll('.event-card')
      .selectAll('.head_volcan')
      .style('background-color', d => {
        if (volcanoSelectedEvents.indexOf(d.event) >= 0) {
          return hexToRGBString(volcanoColor, 0.3)
        } else {
          return hexToRGBString(unselColor, 0.3)
        }
      })

  }
  var reOrderVolcanoes = function () {
    const orderedGroups = d3
      .select('#sourcesPerVolcano')
      .selectAll('.volcan-summary-table')
      .sort(function (a, b) {
        return a.order - b.order;
      });
    orderedGroups.each(function (g, index) {
      d3
        .select(this)
        .raise()
    })
  }

  function addVolcanoSummary(volcanoName, volcanoAllEvents, volcanoSelectedEvents, volcanoColor, volcanoOrder) {
    const volcanoNameClean = cleanName(volcanoName)

    // 1) ADD THE VOLCANO DIV
    const mainDiv = d3.select('#sourcesPerVolcano')
      .selectAll('.nothing')
      .data([{ 'name': volcanoName, 'order': volcanoOrder }])
      .enter()
      .append('div')
      .attr('id', 'summary_' + volcanoNameClean)
      .attr('class', 'volcan-summary-table')
    mainDiv.append('h3').html(volcanoName + ' Volcano')
    loading()

    // 2) ITERATE THROUGH EVENTS TO ADD SUB DIVS
    Object.keys(volcanoAllEvents).forEach(eventName => {
      var event = volcanoAllEvents[eventName]
      d3
        .json(getAnalysisByAuthorsURL(volcanoName, eventName))
        .then((data) => {
          ready()
          var eventColor = unselectedColor()
          if (volcanoSelectedEvents.indexOf(eventName) >= 0) eventColor = volcanoColor

          // 3) GET SUMMARY PER EVENT
          var eventSummary = getEventSummary(data.data)
          if (eventSummary.localeCompare('') == 0) {
            return
          }

          // 4) CREATE ORDER PRIORITY
          var eventAgeOrder = ''
          var eventAge = ''
          var ageText = ''
          if (eventName == 'Unknown') {
            eventAgeOrder = 'd'
          } else if (event.HistoricalAge) {
            eventAgeOrder = 'a'
            eventAge = event.HistoricalAge
            ageText = '(historical age: ' + eventAge + ' c.e.)'
          } else if (event.MinAge14C) {
            eventAgeOrder = 'b'
            eventAge = event.MinAge14C
            ageText = '(14C age [' + event.MinAge14C + ', ' + event.MaxAge14C + '] BP)'
          } else {
            eventAgeOrder = 'c'
            ageText = '(no age)'
          }

          // 5) CREATE DIV PER EVENT
          var eventNameClean = cleanName(eventName)
          var volcanoEventNameClean = volcanoNameClean + '_' + eventNameClean
          const outerDiv = mainDiv
            .selectAll('.nothing')
            .data([{ 'volcano': volcanoName, 'event': eventName, 'eventAgeOrder': eventAgeOrder, 'eventAge': eventAge }])
            .enter()
            .append('div')
            .attr('class', 'card event-card')
          const header = outerDiv
            .append('div')
            .attr('class', 'mb-0 head_volcan head_volcan_' + eventNameClean)
            .attr('id', 'head_' + volcanoEventNameClean)
            .style('background-color', hexToRGBString(eventColor, 0.3))
          header
            .append('button')
            .attr('class', 'btn btn-link')
            .on('click', () => {
              _tef.filter('event', { 'volcano': volcanoName, 'event': eventName })
            })
            .html(d => d.event + " " + ageText)

          // 6) ADD INTERACTIVE CONTROL
          header
            .append('a')
            .attr('id', 'id_b_' + volcanoEventNameClean)
            .attr('class', 'btn')
            .attr('data-toggle', 'collapse')
            .attr('href', '#id_' + volcanoEventNameClean)
            .attr('role', 'button')
            .attr('aria-expanded', false)
            .attr('aria-controls', '#id_' + volcanoEventNameClean)
            .style('float', 'right')
            .html('+')

          outerDiv
            .append('div')
            .attr('id', 'id_' + volcanoEventNameClean)
            .attr('class', 'collapse')
            .append('div')
            .attr('class', 'card card-body')
            .html(eventSummary)

          reOrderEvents()
        })
    })
  }

  function removeVolcanoSummary(volcan) {
    var asdf = document.getElementById(
      'summary_' + cleanName(volcan)
    )
    if (asdf !== null) {
      asdf.remove()
    }
  }

  function getEventSummary(data) {
    var summary = ''
    const summary_dict = {}
    var lastDOI = 'NO_DOI'
    var lastTypeOfSection = 'NO_SECTION'
    var openUl = false
    data.forEach(function (d) {
      // ONLY ADD DOI FOR NEW AUTHOR
      if ((lastTypeOfSection != 'NO_SECTION' && lastTypeOfSection != d.TypeOfSection && openUl) || lastDOI != d.DOI) {
        summary += '</ul>'
        openUl = false
      }
      if (lastDOI != d.DOI) {
        if (d.DOI !== "") {
          summary += '<b> <a href="' + d.DOI + '" target="_blank">' + d.Authors + '</a></b>'
        } else {
          summary += '<b>' + d.Authors + '</b>'
        }
        lastTypeOfSection = 'NO_DOI'
      }
      if (lastTypeOfSection != d.TypeOfSection) {
        const nSections = d.countSection
        summary += ' Analyzed material in ' + nSections + ' '
        if (d.TypeOfSection != '')
          summary += d.TypeOfSection
        else
          summary += 'not specified section'
        if (nSections > 1 && (d.TypeOfSection != "Bog peat"))
          summary += "s"
        summary += ":"
        summary += '<br/>'
        summary += '<ul>'
        openUl = true
      }

      summary += '<li>'
      if (d.TypeOfRegister != '') {
        summary += '(' + d.TypeOfRegister + ') '
      }
      summary += d.AnalyzedMaterial

      var elements = []
      if (d.existMajors == 1) elements.push('Major elements')
      if (d.existTraces == 1) elements.push('Trace elements')
      if (elements.length > 0) {
        summary += ' <br> ' + elements.join(', ') + ': '
      } else {
        summary += '<br> '
      }

      summary += d.count
      if (d.count == 1)
        summary += ' analysis'
      else
        summary += ' analyses'
      summary += '</li>'

      lastDOI = d.DOI
      lastTypeOfSection = d.TypeOfSection

    })
    return summary
  }
  var reOrderEvents = function () {
    const orderedGroups = d3
      .select('#sourcesPerVolcano')
      .selectAll('.volcan-summary-table')
      .selectAll('.event-card')
      .sort(function (a, b) {
        if (a.eventAgeOrder == 'c' && b.eventAgeOrder == 'c') {
          return a.event.localeCompare(b.event)
        }
        if (a.eventAgeOrder > b.eventAgeOrder) return 1
        else if (a.eventAgeOrder < b.eventAgeOrder) return -1
        // IF THEY HAVE THE SAME ORDER PRIORITY
        else {
          if (a.eventAge > b.eventAge) return -1
          if (a.eventAge < b.eventAge) return 1
          return 0;
        }
      });
    orderedGroups.each(function (g, index) {
      d3
        .select(this)
        .raise()
    })
  }
  return table
}
