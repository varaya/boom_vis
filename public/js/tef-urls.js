export function getScatterplotRangeURL(xDim, yDim){
    return '/api/extent?x=' + xDim + '&y=' + yDim
}

export function getAnalysisByAuthorsURL(volcanoName, eventName){
    return '/api/analysis-by-authors?volcano=' + volcanoName + '&event=' + eventName
}

export function getVolcanoesURL(){
    return '/api/volcanoes'
}

export function getEventsURL(volcanoName){
    var url = '/api/events'
    if(volcanoName){
        url += '?volcano=' + volcanoName
    }
    return url
}

export function getSamplesURL(volcanoName){
    var url = '/api/samples'
    if(volcanoName){
        url += '?volcano=' + volcanoName
    }
    return url
}
