var first_time = true;

function escapeRegExp(string) {
  return string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&') // $& means the whole matched string
}

export function replaceAll(str, find, _replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), _replace)
}

export function hexToRGB(hex, opacity) {
  let r = 0, g = 0, b = 0;
  // handling 3 digit hex
  if (hex.length == 4) {
    r = "0x" + hex[1] + hex[1];
    g = "0x" + hex[2] + hex[2];
    b = "0x" + hex[3] + hex[3];
    // handling 6 digit hex
  } else if (hex.length == 7) {

    r = "0x" + hex[1] + hex[2];
    g = "0x" + hex[3] + hex[4];
    b = "0x" + hex[5] + hex[6];
  };
  return [+r / 255, +g / 255, +b / 255, opacity];
}

export function hexToRGBString(hex, opacity) {
  let rgb = hexToRGB(hex, opacity)
  return 'rgba(' + (rgb[0] * 255) + ',' + (rgb[1] * 255) + ',' + (rgb[2] * 255) + ',' + opacity + ')'
}

export function cleanName(name) {
  var r = replaceAll(name, ' ', '_')
  return r
}

export function loading() {
  if (first_time) return
  d3.selectAll('.main-section').style('opacity', 0.7).style('pointer-events', 'none')
  d3.select('#tef-loading').style('visibility', 'visible')
}

export function loading_init() {
  d3.selectAll('.main-section').style('opacity', 0).style('pointer-events', 'none')
  d3.select('#tef-loading').style('visibility', 'visible')
}

export function ready() {
  if (first_time) return
  d3.selectAll('.main-section').style('opacity', 1).style('pointer-events', 'all')
  d3.selectAll('.tef-loading').style('visibility', 'hidden')
}
export function ready_init() {
  d3.select('#tef-loading').style('visibility', 'hidden')
  d3.selectAll('#tef-loading-init').style('visibility', 'visible')
  setTimeout(() => {
    d3.selectAll('#tef-loading-init').style('visibility', 'hidden')
    d3.selectAll('.main-section').style('opacity', 1).style('pointer-events', 'all')
  }, 1500)
  first_time = false;
}

export function toCSV(arr) {
  const columnsStr = 'Volcano;Event;Magnitude;VEI;SampleID;SampleObservationID;ISGN;Location;Latitude;Longitude;Authors;DOI;TypeOfRegister;TypeOfAnalysis;AnalyzedMaterial;AnalyticalTechnique;TypeOfSection;SectionID;SubSectionID;SubSection_DistanceFromTop;HistoricalAge;14C_Age;14C_Age_Error;14CLabCode;StratigraphicPosition;40Ar39Ar_Age;40Ar39Ar_Age_Error;DepositColor;DepositThickness_cm;GrainSize_min;GrainSize_max;SiO2;TiO2;Al2O3;FeO;Fe2O3;Fe2O3T;FeOT;MnO;MgO;CaO;Na2O;K2O;P2O5;Cl;H2O;LOI;Total;Rb;Sr;Y;Zr;Nb;Cs;Ba;La;Ce;Pr;Nd;Sm;Eu;Gd;Tb;Dy;Ho;Er;Tm;Yb;Lu;Hf;Ta;Pb;Th;U;La_Yb;Zr_Nb;87Sr_86Sr;2SE_87Sr_86Sr;143Nd_144Nd;2SE_143Nd_144Nd;MeasurementRun;Comments;Flag;FlagDescription;Map?;SiO2_normalized;TiO2_normalized;Al2O3_normalized;FeO_normalized;Fe2O3_normalized;Fe2O3T_normalized;FeOT_normalized;MnO_normalized;MgO_normalized;CaO_normalized;Na2O_normalized;K2O_normalized;P2O5_normalized;Cl_normalized;Total_normalization'
  var result = columnsStr + '\n'
  const columns = columnsStr.split(';')
  arr.forEach(row => {
    var newLine = ''
    columns.forEach(c => {
      newLine += row[c] + ';'
    })
    result += newLine.slice(0, -1) + '\n'
  })
  return result
}

export function showOnlySelectedVolcanoes () {
  return d3.select('.scatter-controls').selectAll('#scatterSwitch').node().checked
}

export function showUnknownVolcanoSamples () {
  return d3.select('.tef-menu-filters-charts').selectAll('#unknownSwitch').node().checked
}


