export function sampleObservationInfo(d) {
    var tipText = '<h3 style="text-align: center;">Sample Observation </h3>'

    tipText += '<h4> Information </h4>'
    tipText +=
        'Sample Observation ID: <text class="titulo-tooltip">' +
        d.SampleObservationID +
        '</text><br/>'
    tipText +=
        'Sample ID: <text class="titulo-tooltip">' +
        d.SampleID +
        '</text><br/>'
    tipText +=
        'Reference: <text class="titulo-tooltip">'
    if (d.DOI != '') {
        tipText += '<a target="_blank" href="' + d.DOI + '">'
    }
    tipText += d.Authors
    if (d.DOI != '') {
        tipText += '</a>'
    }
    tipText += '</text><br/>'
    tipText +=
        'Measured material: <text class="titulo-tooltip">' +
        d.AnalyzedMaterial +
        '</text><br/>'
    tipText +=
        'Type of section: <text class="titulo-tooltip">' +
        d.TypeOfSection +
        '</text><br/>'
    tipText +=
        'Latlng: <text class="titulo-tooltip"> (' +
        d.Latitude + ',' + d.Longitude + ')' +
        '</text><br/>'
    tipText +=
        'Stratigraphic Position: <text class="titulo-tooltip">' +
        d.StratigraphicPosition +
        '</text><br/>'
    tipText +=
        'Deposit Thickness (cm): <text class="titulo-tooltip">' + d.DepositThickness_cm + '</text><br/>'
    tipText +=
        'Deposit Color: <text class="titulo-tooltip">' + d.DepositColor + '</text><br/>'
    tipText +=
        'Grain size [min, max] (mm): '
    if (d.GrainSize_min_mm != null) {
        tipText += '<text class="titulo-tooltip"> [' + d.GrainSize_min_mm + ',' + d.GrainSize_min_mm + ']</text>'
    }
    tipText += '<br/>'
    tipText +=
        'Age: <text class="titulo-tooltip">'
    //if (d['14C_Age'] != '')
    tipText += d['14C_Age'] + ' ± ' + d['14C_Age_Error'] + '14'.sup() + 'C years BP'
    tipText += ' </text><br/>'
    tipText +=
        'Flag: <text class="titulo-tooltip">' +
        d.Flag +
        '</text><br/>'
    tipText += '<br/>'


    tipText += '<h4> Interpretation </h4>'
    tipText +=
        'Volcano: <text class="titulo-tooltip">' + d.Volcano + '</text><br/>'
    tipText +=
        'Eruption: <text class="titulo-tooltip">' +
        d.Event +
        '</text><br/>'
    tipText +=
        'Magnitude: <text class="titulo-tooltip">' +
        d.Magnitude +
        '</text><br/>'
    tipText +=
        'VEI: <text class="titulo-tooltip">' + d.Vei + '</text><br/>'
    return tipText
}

export function sampleInfo(d, numberObervations) {
    if (d.length == 0) return 'No data'
    var flags = new Set()
    d.forEach(e => {
        e.Flag.forEach(f => {
            flags.add(f)
        })
    })
    flags = Array.from(flags)
    const aSample = d[0]
    var tipText = '<h3 style="text-align: center;"> Sample ' + aSample.SampleID + ' </h3>'

    tipText += '<h4> Information </h4>'
    tipText +=
        'Measured material: <text class="titulo-tooltip">' +
        aSample.AnalyzedMaterial +
        '</text><br/>'
    tipText +=
        'Age: <text class="titulo-tooltip">'
    if (aSample['14C_Age'] != '')
        tipText += aSample['14C_Age'] + ' ± ' + aSample['14C_Age_Error'] + '14'.sup() + 'C years BP'
    tipText += ' </text><br/>'
    tipText +=
        'Reference: <text class="titulo-tooltip">'
    if (aSample.DOI != '') {
        tipText += '<a target="_blank" href="' + aSample.DOI + '">'
    }
    tipText += aSample.Authors
    if (aSample.DOI != '') {
        tipText += '</a>'
    }
    tipText += '</text><br/>'
    tipText +=
        'Latlng: <text class="titulo-tooltip"> (' +
        aSample.Latitude + ',' + aSample.Longitude + ')' +
        '</text><br/>'
    tipText +=
        'Type of section: <text class="titulo-tooltip">' +
        aSample.TypeOfSection +
        '</text><br/>'
    tipText +=
        'Stratigraphic Position: <text class="titulo-tooltip">' +
        aSample.StratigraphicPosition +
        '</text><br/>'
    tipText +=
        'Deposit Thickness (cm): <text class="titulo-tooltip">' + aSample.DepositThickness_cm + '</text><br/>'
    tipText +=
        'Deposit Color: <text class="titulo-tooltip">' + aSample.DepositColor + '</text><br/>'
    tipText +=
        'Grain size [min, max] (mm): '
    if (d.GrainSize_min_mm != null) {
        tipText += '<text class="titulo-tooltip"> [' + aSample.GrainSize_min_mm + ',' + aSample.GrainSize_min_mm + ']</text>'
    }
    tipText += '<br/>'
    tipText +=
        'Sample Observations: <text class="titulo-tooltip">' +
        numberObervations +
        '</text><br/>'
    tipText +=
        'Flags Obs: <text class="titulo-tooltip">' +
        flags +
        '</text><br/>'
    tipText += '<br/>'
    tipText += '<h4> Interpretation </h4>'
    tipText +=
        'Volcano: <text class="titulo-tooltip">' + aSample.Volcano + '</text><br/>'
    tipText +=
        'Eruption: <text class="titulo-tooltip">' +
        aSample.Event +
        '</text><br/>'
    return tipText
}
/*export function sampleObservationInfo(d) {
    const aSample = d
    var tipText = '<h3 style="text-align: center;"> Sample Observation' + aSample.SampleObservationID + ' </h3>'

    tipText += '<h4> Information </h4>'
    tipText +=
        'Measured material: <text class="titulo-tooltip">' +
        aSample.AnalyzedMaterial +
        '</text><br/>'
    tipText +=
        'Age: <text class="titulo-tooltip">'
    if (aSample['14C_Age'] != '')
        tipText += aSample['14C_Age'] + ' ± ' + aSample['14C_Age_Error'] + '14'.sup() + 'C years BP'
    tipText += ' </text><br/>'
    tipText +=
        'Reference: <text class="titulo-tooltip">'
    if (aSample.DOI != '') {
        tipText += '<a target="_blank" href="' + aSample.DOI + '">'
    }
    tipText += aSample.Authors
    if (aSample.DOI != '') {
        tipText += '</a>'
    }
    tipText += '</text><br/>'
    tipText +=
        'Type of section: <text class="titulo-tooltip">' +
        aSample.TypeOfSection +
        '</text><br/>'
    tipText +=
        'Stratigraphic Position: <text class="titulo-tooltip">' +
        aSample.StratigraphicPosition +
        '</text><br/>'
    tipText +=
        'Original latlng: <text class="titulo-tooltip"> (' +
        aSample.Latitude + ',' + aSample.Longitude + ')' +
        '</text><br/>'
    tipText +=
        'Sample Observations: <text class="titulo-tooltip">' +
        numberObervations +
        '</text><br/>'
    tipText +=
        'Flags Obs: <text class="titulo-tooltip">' +
        aSample.Flag +
        '</text><br/>'
    tipText += '<br/>'
    tipText += '<h4> Interpretation </h4>'
    tipText +=
        'Volcano: <text class="titulo-tooltip">' + aSample.Volcano + '</text><br/>'
    tipText +=
        'Eruption: <text class="titulo-tooltip">' +
        aSample.Event +
        '</text><br/>'
    return tipText
}*/

export function eventInfo(d) {
    var tipText = '<h4 style="text-align: center;"> Eruption ' + d.Name + '</h4>'
    tipText +=
        'Volcano: <text class="titulo-tooltip">' + d.Volcano + '</text><br/>'
    tipText +=
        'Age range: <text class="titulo-tooltip">' +
        Math.abs(d.MinAge14C) +
        '—' +
        Math.abs(d.MaxAge14C) +
        ' 14'.sup() + 'C years BP</text><br/>'
    tipText +=
        'Magnitude: <text class="titulo-tooltip">' +
        (d.Magnitude ? d.Magnitude : '') +
        '</text><br/>'
    tipText +=
        'VEI: <text class="titulo-tooltip">' +
        (d.Vei ? d.Vei : '') +
        '</text><br/>'
    return tipText
}