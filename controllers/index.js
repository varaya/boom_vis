var db = require("../database.js")

exports.init = async (req, res) => {
    try {
        return res.render('index.ejs');
    } catch (error) {
        if (error) throw error;
    }
}

exports.getVolcanoes = async (req, res) => {
    var sql = "select * from volcano order by Latitude desc"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "message": "success",
            "data": rows
        })
    });
}

exports.getEvents = async (req, res) => {
    var query = req.query;
    var sql = "select * from event "
    if ('volcano' in query)
        sql += "where Volcano like '" + query.volcano + "'"
    sql += "order by HistoricalAge DESC"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "message": "success",
            "data": rows
        })
    });
}
exports.getSamples = async (req, res) => {
    var query = req.query;
    // ONLY RETURN NECESSARY COLUMNS TO REDUCE THE SIZE OF THE QUERY
    var columns = `SiO2_normalized, TiO2_normalized, Al2O3_normalized, FeO_normalized,
    Fe2O3_normalized, Fe2O3T_normalized, FeOT_normalized, MnO_normalized, 
    MgO_normalized, CaO_normalized, Na2O_normalized, K2O_normalized, Cl_normalized,
    Total_normalization, Rb, Sr, Y, Zr, Nb, Cs,
    Ba, La, Ce, Pr, Nd, Sm, Eu, Gd,
    Tb, Dy, Ho, Er, Tm, Yb, Lu, Hf,
    Ta, Pb, Th, U, 
    La_Yb, Zr_Nb, "87Sr_86Sr", "143Nd_144Nd",
    SampleObservationID, SampleID, Authors, AnalyzedMaterial, 
    TypeOfSection, StratigraphicPosition, "14C_Age", "14C_Age_Error", "14C_Age_Num", "14C_Age_Error_Num", 
    Flag, Volcano, Event, Magnitude, Vei, DOI, Latitude, Longitude, 
    AnalyzedMaterial, TypeOfSection, TypeOfRegister, TypeOfAnalysis, StratigraphicPosition,
    DepositThickness_cm, DepositColor, GrainSize_min_mm, GrainSize_max_mm `
    var sql = "select " + columns + " from sample "
    if ('volcano' in query && !('event' in query)) {
        sql += "where Volcano like '" + query.volcano + "'"
    } else if ('volcano' in query && 'event' in query) {
        sql += "where Volcano like '" + query.volcano + "' and Event like '" + query.event + "'"
    } /*else {
        // FOR TESTING
        sql += "where Volcano like 'LLaima' limit 100"
    }*/
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        // ORIGINAL CODE TO RETURN JSON FORMAT
        res.json({
            "message": "success",
            "data": rows
        })

        // CODE TO RETURN CSV. IN THEORY LIGHTER
        /*const jsonexport = require('jsonexport');
        jsonexport(rows, { rowDelimiter: ';' }, function (err, csv) {
            if (err) return console.error(err);
            return res.status(200).send(csv);
        });*/
    });
}

exports.getExtent = async (req, res) => {
    var query = req.query;
    var x = query.x
    var y = query.y
    const toIgnore = '("n.a.", "Not analyzed", "-", "Not determined", "n.d", "n.d.", "<0.01", "<0.1", "<1", "<5", "<6", "<10", "Over range", "bdl", "")'
    var sql = 'select MIN("' + x + '") as min_x, MAX("' + x + '") as max_x, MIN("' + y + '")as min_y,'
    sql += ' MAX("' + y + '") as max_y from sample where "'
    sql += x + '" NOT IN ' + toIgnore + ' AND "' + y + '" NOT IN ' + toIgnore
    sql += ' AND Flag NOT LIKE "%Outlier%"'
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.json({
            "message": "success",
            "data": rows
        })
    });
}

exports.getAnalysisByAuthors = async (req, res) => {
    // This query might be easier in MongoDB
    // I'm doing a manual join because I was not
    // able to find a decent way to do this in SQLite
    var query = req.query;
    var v = query.volcano
    var e = query.event
    var sql = "select Volcano, Event, Authors, DOI, DOIYear, TypeOfAnalysis, TypeOfRegister, TypeOfSection, AnalyzedMaterial, "
    sql += "(SiO2 or TiO2 or Al2O3 or FeO or Fe2O3 or Fe2O3T or FeOT or MnO or MgO or CaO or Na2O or K2O or Cl or Total or SiO2_normalized or TiO2_normalized or Al2O3_normalized or FeO_normalized or Fe2O3_normalized or Fe2O3T_normalized or FeOT_normalized or MnO_normalized or MgO_normalized or CaO_normalized or Na2O_normalized or K2O_normalized or Cl_normalized or Total_normalization) as existMajors, "
    sql += "(Rb or Sr or Y or Zr or Nb or Cs or Ba or La or Ce or Pr or Nd or Sm or Eu or Gd or Tb or Dy or Ho or Er or Tm or Yb or Lu or Hf or Ta or Pb or Th or U) as existTraces, "
    sql += "count(*) AS count, "
    sql += "count(DISTINCT Latitude) as countSection from sample "
    sql += "where Volcano like '" + v + "' and Event like '" + e + "' "
    sql += "group by Authors, TypeOfSection, TypeOfRegister, TypeOfAnalysis, AnalyzedMaterial "
    sql += "order by DOIYear DESC, Authors ASC, TypeOfSection ASC, TypeOfRegister ASC, TypeOfAnalysis ASC"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        var query2 = "Select Authors, TypeOfSection, count(DISTINCT Latitude) as countSection"
        query2 += " from sample "
        query2 += " where Volcano like '" + v + "' and Event like '" + e + "'"
        query2 += " group by Authors, TypeOfSection "
        db.all(query2, params, (err, rows2) => {
            if (err) {
                res.status(400).json({ "error": err.message });
                return;
            }
            const countSection = {}
            rows2.forEach(r => {
                if (!(r.Authors in countSection)) {
                    countSection[r.Authors] = {}
                }
                countSection[r.Authors][r.TypeOfSection] = r.countSection
            })
            rows.forEach(r => {
                r.countSection = countSection[r.Authors][r.TypeOfSection]
            })
            res.json({
                "message": "success",
                "data": rows
            })
        });
    });
}

