// Require 
//require('dotenv').config();
const express = require('express');
const ejs = require('ejs');
const path = require('path');
const morgan = require('morgan');
const helmet = require('helmet');
// App
const app = express();

var db = require("./database.js")

// Middlewares
app.use(morgan('combined'));
// View engine ejs
app.set('view engine', 'ejs');


// Public static folder
app.use(express.static(path.join(__dirname, 'public')));
app.use('/js', express.static(path.join(__dirname, 'public/js/libs')));
app.use('/css', express.static(path.join(__dirname, 'public/css')));
// Adding css and js files from installed apps
app.use('/js', express.static(path.join(__dirname, 'node_modules/jquery/dist')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/d3/dist')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/leaflet/dist')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/crossfilter2/')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/dc/dist')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/dc.leaflet')))
app.use('/js', express.static(path.join(__dirname, 'node_modules/papaparse')))

app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')))
app.use('/css', express.static(path.join(__dirname, 'node_modules/leaflet/dist/')))
app.use('/css', express.static(path.join(__dirname, 'node_modules/dc/dist/style')))


// Body Parser
app.use(express.urlencoded({
    extended: false
}));
app.use(express.json());
app.use(helmet());
// External routes
const index = require('./routes/index');
app.use('/', index);
app.use('/static', express.static(__dirname + '/node_modules'));

// Listener
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
    console.log(`Server running on PORT ${PORT}`);
});